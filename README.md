# Proximity Lock

Lock & unlock an Encrypted OS when your mobile or zring comes in range of it's bluetooth - using Preboot Initramfs modules & SSH!
- or a Policy requiring multiple people in same room! i.e. SCIFFs

 
- Sch: https://www.google.com/search?q=kernel+luks+initrd+ssh
- guide: https://hamy.io/post/0005/remote-unlocking-of-luks-encrypted-root-in-ubuntu-debian/
- wiki: https://wiki.archlinux.org/index.php/Dm-crypt/Specialties